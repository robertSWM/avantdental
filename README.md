# README #

Added some scripts/styles to run in scratchpad of http://www.avantdental.com.au/

1. Go a page on http://www.avantdental.com.au/ in Firefox (with scratchpad enabled)
2. Load the make-responsive.js file, then Run
3. Make your browser window smaller and watch the responsive magic

If you are having trouble loading the mobile.js and mobile.css files, change them to another hosted reference (such as localhost if you have that running)