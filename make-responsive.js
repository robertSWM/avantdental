//var mylocal = 'http://localhost/bam/avantdental/';
var mylocal = 'http://roberto.dev1.bam.com.au/avantdental/';
var cacheBust = Date.now();

//add jquery
var j = document.createElement('script');
j.type = 'text/javascript';
j.src = 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js';

//add boostrap js
var bs = document.createElement('script');
bs.type = 'text/javascript'
bs.src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js';

//add bootstrap styles
var bss = document.createElement('link');
bss.href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css';
bss.rel = 'stylesheet';
bss.media = '(max-width:1024px)';

//add meta tags for devices
var meta1 = document.createElement('meta');
meta1['http-equiv'] = 'X-UA-Compatible';
meta1.content = 'IE=edge';
var meta2 = document.createElement('meta');
meta2.name = 'viewport';
meta2.content = 'width=device-width, initial-scale=1';

var mj = document.createElement('script');
mj.type = 'text/javascript';
mj.src = mylocal + 'mobile.js?' + cacheBust;

var ms = document.createElement('link');
ms.href = mylocal + 'mobile.css?' + cacheBust;
ms.rel = 'stylesheet';
ms.media = '(max-width:1024px)';

//attach extras to dom
var head = document.head || document.getElementsByTagName('head')[0];
head.appendChild(meta1);
head.appendChild(meta2);
head.appendChild(bss);
head.appendChild(ms);
document.body.appendChild(j);
document.body.appendChild(bs);
setTimeout(function(){
	document.body.appendChild(mj);
}, 1500);
