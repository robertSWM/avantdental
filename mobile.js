var buildNav = function(){
  if($(window).width() > 1024){
    $('#mobileSurround').remove();
    $('#mobileContainer').removeClass('container-fluid');
  }else{
    if(!$('#mobileSurround').length){
      $('body').prepend('<nav class="navbar navbar-default" id="mobileSurround">' +
               '<div class="container-fluid">' +
               '<div class="navbar-header">' +
               '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobileBox" aria-expanded="false">' +
               '<span class="sr-only">Toggle navigation</span>' +
               '<span class="icon-bar"></span>' +
               '<span class="icon-bar"></span>' +
               '<span class="icon-bar"></span>' +
               '</button>' +
               '<a class="navbar-brand" href="/">Site Logo</a>' +
               '</div>' +
               '<div class="collapse navbar-collapse" id="mobileBox">' +
               '<ul class="nav navbar-nav" id="mobileNav"></ul>' +
               '</div>' +
               '</div>' +
               '</nav>');

      $('#nav ul > ').clone().appendTo('#mobileNav');
      $('#mobileContainer').addClass('container-fluid');
    }
  }
};
$(document).ready(function(){
  $('#container').wrap('<div id="mobileContainer"></div>');
  buildNav();
});

$(window).resize(function(){
  buildNav();
});
